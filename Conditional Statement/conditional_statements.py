"""
a = 28
b = 26

if a > b:
	print("A is greater than B")
	print("Inside if")

print("Outside if")

print("Program Executed")

"""

"""
Comment
a = 28
b = 26

if a > b:
	print("A Greater")
else:
	print("B Greater")

print("Executed")
"""

"""
num1 = input("Number-1: ")
num2 = input("Number-2: ")

print(num1, num2)
print(type(num1), type(num2))

num1 = int(num1)
num2 = int(num2)

print(type(num1), type(num2))

if num1 > num2:
	print("Number-1 Greater")
else:
	if num1 == num2:
		print("They are equal")
	else:
		print("Number-2 Greater")

"""

# HomeTask -> Num1, Num2, Num3

