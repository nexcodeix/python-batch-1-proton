"""
a = 0

while a < 10:
	print("This is from loop")
	print(a)
	a = a + 1
"""

"""a = 5
rem = a % 2
print("Remainder " + str(rem))
is_even = (rem == 0)
print("Is even: ", is_even)"""

"""
a = 0

while a <= 20:  # Con1 -> a < 20 and Condition 2 -> a == 20 
	# Checking if a is even
	print("Variable a: " + str(a))
	rem = a % 2
	if rem == 0:
		print("This is an even number")
	else:
		print("This is an odd number")

	a = a + 1

"""
