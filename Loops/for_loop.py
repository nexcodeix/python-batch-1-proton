# range_function = range(10)  # 0 --> 10 0, 1, 2 ... 9

"""
[0, 1, 2, 3, 4]

for loop {
	b = first_item
}

for loop {
	b = second_item
}

"""


for b in range(10):
	b = str(b)
	print("B becomes " + b)  # string concatenation
