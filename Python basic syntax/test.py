"""
a = "This is from sublime"
b = "This is from notepad"

print(a + " " + b)  # This is a concatenation     

"""

# This is a comment

"""
THIS IS A LONG COMMENT
"""


a = input("Type the first number: ")
b = input("Type the Second number: ")

a = int(a)
b = int(b)

if a == 0:
	print("A is equal to 0")
	print("Please Type number other than 0")

else:
	if b == 0:
		print("B is equal to 0")
		print("Please Type number other than 0")
	else:
		print("First output: " + str(a/b))
		print("Second Output: " + str(b/a))

