empty_list = []

list_length = input("Length of list: ")  # 5
list_length = int(list_length)

# while loop
# a = 1
# while a <= list_length:  # a<5 or a==5 not a>5
# 	data = input("Enter a Data: ")
# 	empty_list.append(data)  # adding data to list
# 	a = a + 1

# for loop
# for i in range(list_length):  # range(5)  --> 0, 4
# 	data = input("Enter a Data: ")
# 	empty_list.append(data)  # adding data to list	

print(empty_list)
