# word --> key, meaning --> value

bio = {
	"name": "John",  # Pair
	"age": 10,
	"height": 5.5,
}

# print(bio)

#  print(bio.get("name"))  # getting an attribute/key

# print(bio["age"])  # getting by index

bio["age"] = bio["age"] + 5  # updating

# print("Updated Dictionary ", bio)


# tuple

t = ("a", "b", "c", 10, 20, 30)
print(t)

"""
copy()	Returns a copy of the list
count()	Returns the number of elements with the specified value
extend()	Add the elements of a list (or any iterable), to the end of the current list
index()	Returns the index of the first element with the specified value
reverse()	Reverses the order of the list
sort()	Sorts the list
"""

