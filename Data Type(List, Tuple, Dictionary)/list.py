a = [10, 20, 30, 50, 60, "string1", "string2"]  # list (class)
# a = []
print(a)
# indexing starts from 0

print(a[0]) # gives first element

# Finding length of a list

counter = len(a)  # Length
length_of_list = a.__len__()
print(length_of_list)

# Appending (Adding Something)

a.append("meat") # add to list
a.append(90)

# Removing 

a.remove("string1")  # removing from list
a.pop(3)  # removing through index



# a.remove(0)  # 0 gives error as it's not in the list

# a.clear()  # removes all the elements in the list

print(a)


# user will put data and at the end of programe you will show 
# the list to user
