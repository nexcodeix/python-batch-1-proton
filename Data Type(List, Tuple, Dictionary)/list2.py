a = [10, 20, 30, 50, 60, 70, 20, 40, 60, 55]
list2 = ["D", "b", "t", "a", "q", "r"]
print(a)

# Positive-indexing

# value1 = a[3]  # getting
# value2 = value1 + 100
# # print(value2)

# a[3] = value2  # setting inserting value2 in the memory location of a[3]

# # a[4] = "string 3"

# # Negative Indexing

# # print(a[-5])

# a[-5] = 40  # updating the index of -5


# Slicing

# print(a[0:4])
# print(a[0:5])

first_list = a[0:6]  # making a new list from list a
# print("First List: ", first_list)

second_list = a[6:]  # starting from 6 till end of list
# print("second_list: ",second_list)


# List Functions

# Getting Index of a Value
# index_of_value = a.index("string1")

# Copying a List

b = a.copy()
b = a[0:]
# print("Copied List", b)

# Counting value

couting_value = a.count(20)  # Counting the value


# reversing list

# a.reverse() # reversing the list

# Sorting List

# a.sort()
list2.sort()

# Inserting
a.insert(2, 10)

print("Updated List ", a)

# Counting values

"""
append()	Adds an element at the end of the list
clear()	Removes all the elements from the list
copy()	Returns a copy of the list
count()	Returns the number of elements with the specified value
extend()	Add the elements of a list (or any iterable), to the end of the current list
index()	Returns the index of the first element with the specified value
insert()	Adds an element at the specified position
pop()	Removes the element at the specified position
remove()	Removes the item with the specified value
reverse()	Reverses the order of the list
sort()	Sorts the list

"""
